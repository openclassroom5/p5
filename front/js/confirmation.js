
// Depuis Utils.js pour définir le numero de commande
const orderId = getParamById('id')

// Si le numero de commande est différent d'undefined alors on affiche le numero.
if (orderId !== undefined) {
    const idElement = document.getElementById('orderId')
    idElement.innerText = orderId

    //Puis on clear le localstorage.
    localStorage.clear()
}