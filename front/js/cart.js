const section = document.getElementById("cart__items")
const productTotalQuantity = document.getElementById('totalQuantity')
const productTotalPrice = document.getElementById('totalPrice')

//-------------------------------------
// Display du Panier depuis le LocalStorage
//-------------------------------------
const getCart = () => JSON.parse(localStorage.getItem('cart')) || []

const displayCart = () => {
    const cart = getCart()

    cart.forEach(element => {
        fetch(`http://localhost:3000/api/products/${element.id}`)
            .then(response => response.json())
            .then(product => {


                //-------------------------------------
                // Création des elements HTML
                //-------------------------------------

                //Création de l'Article.
                const article = document.createElement('article')

                //Création de la Div comprennant les elements suivant: IMG
                const cartItemImg = document.createElement('div')
                cartItemImg.className = "cart__item__img"

                //Création de l'élément IMG
                const img = document.createElement('img')
                img.src = product.imageUrl
                cartItemImg.appendChild(img) //Dans la Div parent ( img )

                //Création de la Div contenant les éléments suivant: DIV - H2 - P - P
                const cartItemContent = document.createElement('div')
                cartItemContent.className = "cart__item__content"

                //Création de la Div contenant les éléments suivant: H2 - P - P
                const cartItemContentDescription = document.createElement('div')
                cartItemContentDescription.className = "cart__item__content_description"

                //Création du Titre pour le nom des items.
                const itemName = document.createElement('h2')
                itemName.innerText = product.name
                cartItemContentDescription.appendChild(itemName) //Dans la div parent ( description )

                //Création du texte pour indiquer la couleur choisit.
                const itemColor = document.createElement('p')
                itemColor.innerText = element.color
                cartItemContentDescription.appendChild(itemColor) //Dans la div parent ( description )

                //Création du texte pour indiquer le prix de l'item choisit.
                const itemPrice = document.createElement('p')
                itemPrice.innerText = `${product.price},00 €`
                cartItemContentDescription.appendChild(itemPrice) //Dans la div parent ( description )

                cartItemContent.appendChild(cartItemContentDescription) //Dans la div parent ( content )

                //Création de la div contenant les éléments suivant : DIV - P - INPUT
                const cartItemContentSettings = document.createElement('div')
                cartItemContentSettings.className = "cart__item__content__settings"

                //Création de la div contenant les éléments suivant : P - INPUT
                const cartItemContentSettingsQuantity = document.createElement('div')
                cartItemContentSettingsQuantity.className = "cart__item__content__settings__quantity"

                //Création du text pour indiquer la quantité de l'item choisit
                const pQuantity = document.createElement('p')
                pQuantity.innerText = "Qté : "
                cartItemContentSettingsQuantity.appendChild(pQuantity) //Dans la div parent ( quantity )

                //Création de l'input pour selectionner la quantity
                const inputQuantity = document.createElement('input')
                inputQuantity.className = "itemQuantity"
                inputQuantity.type = "number"
                inputQuantity.name = "itemQuantity"
                inputQuantity.min = 1
                inputQuantity.max = 100
                inputQuantity.value = element.quantity

                //Event pour update la quantité selectionner : Remplacer l'ancienne par la nouvelle
                inputQuantity.addEventListener('change', () => {
                    //ParseInt pour avoir un number et non une string.
                    let modifiedQuantity = parseInt(inputQuantity.value)
                    //Quantité de l'élément = Quantité Modifié
                    element.quantity = modifiedQuantity;
                    //Renvoi le cart Update avec la nouvelle quantité
                    localStorage.setItem('cart', JSON.stringify(cart))

                    updateTotals(cart, product)

                })
                cartItemContentSettingsQuantity.appendChild(inputQuantity) //Dans la div parent ( quantity )

                cartItemContentSettings.appendChild(cartItemContentSettingsQuantity) //Dans la div parent ( settings )

                //Création de la div contenant les éléments suivant : P
                const cartItemContentSettingsDelete = document.createElement('div')
                cartItemContentSettingsDelete.className = "cart__item__content__settings__delete"

                //Création du texte pour la suppression d'un article
                const pDeletItem = document.createElement('p')
                pDeletItem.className = "deleteItem"
                pDeletItem.innerText = "Supprimer"
                cartItemContentSettingsDelete.appendChild(pDeletItem) //Dans la div parent ( delete )

                //Event pour supprimer l'article selectionner : Filtre de l'item à suppr puis update du panier via remplacement.
                pDeletItem.addEventListener('click', () => {
                    console.log(element);

                    //Sélectionne l'ID et la Couleur
                    const deleteId = element.id
                    const deleteColor = element.color;

                    //Filtre l'élément
                    const newCart = cart.filter(delet => delet.id !== deleteId || delet.color !== deleteColor);

                    //Envoi les nouvelles données dans le localStorage
                    localStorage.setItem('cart', JSON.stringify(newCart));

                    //Message de suppression
                    alert(" L'article a bien été supprimer ");

                    //Si Localstorage vide alors Panier vide
                    if (newCart.length === 0) {
                        localStorage.clear();
                    }
                    updateCart()
                })

                cartItemContentSettings.appendChild(cartItemContentSettingsDelete)
                cartItemContent.appendChild(cartItemContentSettings)
                article.appendChild(cartItemImg)
                article.appendChild(cartItemContent)
                section.appendChild(article)

                updateTotals(cart, product)

            })
    });
}
displayCart()

// MàJ du total du panier : Quantité & Prix
const updateTotals = (cart, product) => {

    let totalQuantity = 0;
    let totalPrice = 0;

    cart.forEach(element => {

        totalQuantity += element.quantity
        totalPrice += (product.price * element.quantity)

        productTotalQuantity.innerHTML = totalQuantity
        productTotalPrice.innerHTML = `${totalPrice.toLocaleString()},00` //Affiche le prix avec la syntaxe 000 000,00

    })
}

// MàJ du cart uniquement sans reload la page
const updateCart = () => {
    section.innerHTML = ""
    displayCart()
}

//-------------------------------------
// Formulaire
//-------------------------------------

//Formulaire avec Regex

const form = document.querySelector(".cart__order__form");

//Création des expressions
const charaRegExp = /^[a-zA-Z ,.'-]+$/;
const addressRegExp = /^[^@&"()!_$*€£`+=\/;?#]+$/;
const emailRegExp = /^[^\s@]+@[^\s@]+\.[^\s@]+$/

// Selection le bouton pour commander.
const orderElement = document.getElementById('order')
orderElement.disabled = true

// Etat de base du formulaire avant complétion.
const isValidForm = {
    firstName: false,
    lastName: false,
    address: false, 
    city: false,
    email: false,
}

// Permet d'activer le bouton uniquement si le formulair est remplie. 
const isValidInput = (id, regexToCheck, minLength, errorMsgId, msgText) => {

    const inputText = form[id].value

    const isValidText = regexToCheck.test(inputText)
    const errorMsgElm = document.getElementById(errorMsgId)
    if (isValidText && inputText.length > minLength) {
        errorMsgElm.innerText = ""
        isValidForm[id] = true
    } else {
        errorMsgElm.innerText = `${msgText} n'est pas valide`
        isValidForm[id] = false
    }
 console.log(isValidForm);
     orderElement.disabled = !isValidForm["firstName"] || !isValidForm["lastName"] || !isValidForm["address"] || !isValidForm["city"] || !isValidForm["email"]
    }

    
// Valide à chaque changement, chaque input.

// Prénom
const validFirstName = form["firstName"].addEventListener('input', () => { 
    isValidInput("firstName", charaRegExp, 2, "firstNameErrorMsg", "Le prénom")
})
// Nom
const validLastName = form["lastName"].addEventListener('input', () => { 
    isValidInput("lastName", charaRegExp, 2, "lastNameErrorMsg", "Le nom")
})
// Ville
const validCity = form["city"].addEventListener('input', () => { 
    isValidInput("city", charaRegExp, 2, "cityErrorMsg", "La ville")
})
// Addresse
const validAddress = form["address"].addEventListener('input', () => { 
    isValidInput("address", addressRegExp, 5, "addressErrorMsg", "L'adresse")
})
// Email
const validEmail = form["email"].addEventListener('input', () => { 
    isValidInput("email", emailRegExp, 5, "emailErrorMsg", "L'email")
})
// Vérifie que CHAQUE input est correct.
if (validFirstName && validLastName && validCity && validAddress && validEmail) {
    console.log("ok tout est valide");
}

//-------------------------------------
// Confirmation
//-------------------------------------

//Event pour valider la commande
orderElement.addEventListener('click', (event) => {
    event.preventDefault()

    // Récupération des donnés du formulaire
    const contact = {
        firstName: document.getElementById('firstName').value,
        lastName: document.getElementById('lastName').value,
        address: document.getElementById('address').value,
        city: document.getElementById('city').value,
        email: document.getElementById('email').value,
    }
    console.log(contact);

    //Récupération des ID des produits du localStorage
    const cart = getCart()
    const ids = cart.map(item => item.id)

    // Contact + Produits (sendFormData)
    const formData = {
        contact,
        products: ids,
    }

    // (formData) envoyé au serveur
    const sendFormData = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',

        },
        body: JSON.stringify(formData),
    };

    fetch("http://localhost:3000/api/products/order", sendFormData)
        .then(response => response.json())
        .then(data => {
            console.log(data);
            document.location.href = 'confirmation.html?id=' + data.orderId;
        })
        .catch(error => console.log(error))


});