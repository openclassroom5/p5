// Récupère l'ID dans l'URL pour Confirmation.js : défini le numero de commande
const getParamById = (param) => {

    const searchParams = new URLSearchParams(window.location.search)
    return searchParams.get(param)

}

