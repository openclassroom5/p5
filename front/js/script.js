//-------------------------------------
// Récupère l'API 
//-------------------------------------

fetch("http://localhost:3000/api/products")
    .then(response => response.json())
    .then(products => {

        //-------------------------------------
        // Création des elements HTML
        //-------------------------------------

        // Création de chaque éléments pour l'item en utilisant les données de l"API.
        const section = document.getElementById("items")
        products.forEach(product => {

            // Création du lien clickable lié à l'ID du Produit.
            const a = document.createElement("a")
            a.href = `./product.html?id=${product._id}`

            // Création de l'article comportant les créations suivante.
            const article = document.createElement("article")

            // Création des images avec URL et les ALT.
            const img = document.createElement("img")
            img.src = product.imageUrl
            img.alt = product.altTxt

            // Création des Titre H3 et ajout de la classe CSS.
            const h3 = document.createElement("h3")
            h3.innerText = product.name
            h3.className = "productName"

            // Création des Descriptions P et ajout de la classe CSS.
            const p = document.createElement("p")
            p.innerText = product.description
            p.className = "productDescription"

            // Indexion des élements parent - enfant
            article.appendChild(img)
            article.appendChild(h3)
            article.appendChild(p)
            a.appendChild(article)
            section.appendChild(a)
        });

    });