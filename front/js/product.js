//-------------------------------------
// Récupère l'ID dans l'URL du produit
//-------------------------------------

const canape_id = getParamById("id")

//-------------------------------------
// API utilise l'ID du produit choisit
//-------------------------------------

fetch(`http://localhost:3000/api/products/${canape_id}`)
    .then(response => response.json())
    .then(product => {

        // Création de l'image via l'ID de l'url et ajouter l'ALT correspondant.
        const img = document.querySelector(".item__img")
        img.innerHTML = `<img src="${product.imageUrl}" alt="${product.altTxt}">`;

        // Création de l'élément Titre H1 via les données (.name) de l'API
        const h1 = document.getElementById("title")
        h1.innerText = product.name

        // Création de l'élément Description via les données (.description) de l'API
        const p = document.getElementById("description")
        p.innerText = product.description

        // Création de l'élément Prix via les données (.price) de l'API
        const price = document.getElementById("price")
        price.innerText = product.price

        // Création de choix des couleurs disponible (.colors) via les données de l'API
        const color = document.getElementById("colors");
        for (i = 0; i < product.colors.length; i++) {
            color.innerHTML += `<option value="${product.colors[i]}">${product.colors[i]}</option>`;
        }

        //---------------------------------------------
        // Ajouter au panier via le bouton 
        //---------------------------------------------

        // Selectionne le bouton 
        const addToCartButton = document.querySelector("#addToCart")

        // Ajoute un listenner au bouton 
        addToCartButton.addEventListener('click', () => {

            // Récupère la Quantité et la Couleur selectionné
            const selectedQuantity = parseInt(document.querySelector("#quantity").value)
            const selectedColor = document.querySelector("#colors").value

            // Récupère les infos du Cart dans le LocaleStorage
            const cart = JSON.parse(localStorage.getItem("cart")) || []
            console.log({
                cart,
                selectedColor,
                selectedQuantity
            });

            // Vérifie la Quantité et la Couleur de l'article à ajouter 
            if (selectedColor && selectedQuantity > 0 && selectedQuantity <= 100) {
                const itemInCart = cart.find(item => item.id === canape_id && item.color === selectedColor)

                // Si l'article existe déjà alors on met à jour la quantité 
                if (itemInCart !== undefined) {
                    itemInCart.quantity = selectedQuantity
                    // Sinon, on créer un nouvel article    
                } else {
                    const newItem = {
                        id: canape_id,
                        color: selectedColor,
                        quantity: selectedQuantity
                    }
                    cart.push(newItem)
                }
                localStorage.setItem("cart", JSON.stringify(cart))

            }

        })

    })